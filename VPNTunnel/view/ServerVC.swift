//
//  ServerVC.swift
//  VPNTunnel
//
//  Created by Kartik Jasolia on 12/05/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
//

import UIKit

class ServerVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var purchageView: UIView!
    @IBOutlet weak var freeBackView: UIImageView!
    @IBOutlet weak var paidBackView: UIImageView!
    
    let serverName = ["Canada", "NewYork", "Germany", "UK"]
    let serverAddressArr = ["138.197.175.208", "157.245.85.83", "161.35.20.210", "178.62.64.123"]
    let serverUser = ["root", "root", "root", "root"]
    let serverPass = ["123@Diya", "123@Diya", "123@Diya", "123@Diya"]
    
    //paid = 1, free = 0
    var serverFlag = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        freeBackView.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if purchageStatusApp{
            purchageView.isHidden = true
        }
    }
    
    @IBAction func freeBtnTap(_ sender: Any) {
        serverFlag = 0
        freeBackView.isHidden = false
        paidBackView.isHidden = true
        tableView.reloadData()
        scroll()
    }
    
    @IBAction func paidBtnTap(_ sender: Any) {
        serverFlag = 1
        freeBackView.isHidden = true
        paidBackView.isHidden = false
        tableView.reloadData()
        scroll()
    }
    
    @IBAction func premiumUnlockBtnTap(_ sender: Any) {
        let subscriptionVc = self.storyboard?.instantiateViewController(withIdentifier: "SubscribeVC") as! SubscribeVC
        
        self.navigationController?.pushViewController(subscriptionVc, animated: true)
    }

    @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ServerVC: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if serverFlag == 1{
            return serverName.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServerCell", for: indexPath) as! ServerCell
        if serverFlag == 1{
            tableView.separatorStyle = .singleLine
            cell.img.image = UIImage(named: serverName[indexPath.row])
            cell.name.text = "\(serverName[indexPath.row])"
        }else{
            tableView.separatorStyle = .none
           cell.img.image = UIImage(named: serverName[1])
           cell.name.text = "\(serverName[1])"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("tap: \(indexPath.row)")
        if serverFlag == 1{
            freeServerSelectFlag = false
            serverSelectFlag = true
            serverAddress = serverAddressArr[indexPath.row]
            ovpnFile = serverName[indexPath.row]
            username = serverUser[indexPath.row]
            pass = serverPass[indexPath.row]
        }else{
            freeServerSelectFlag = true
            serverSelectFlag = true
            serverAddress = serverAddressArr[1]
            ovpnFile = serverName[1]
            username = serverUser[1]
            pass = serverPass[1]
        }
        UserDefaults.standard.set(freeServerSelectFlag, forKey: "freeServerSelectFlag")
        UserDefaults.standard.set(serverAddress, forKey: "serverAddress")
        UserDefaults.standard.set(ovpnFile, forKey: "ovpnFile")
        UserDefaults.standard.set(username, forKey: "username")
        UserDefaults.standard.set(pass, forKey: "pass")
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func scroll(){
        self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
}
