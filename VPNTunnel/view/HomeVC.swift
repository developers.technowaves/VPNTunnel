//
//  HomeVC.swift
//  VPNTunnel
//
//  Created by Kartik Jasolia on 11/05/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
//

import UIKit
import NetworkExtension
import SpeedcheckerSDK
import CoreLocation

class HomeVC: UIViewController, InternetSpeedTestDelegate {
    
    var finishDownloadStats = false
    var finishUploadStats = false
    
    
    @IBOutlet weak var uploadSpeedLbl: UILabel!
    @IBOutlet weak var downloadSpeedLbl: UILabel!
    @IBOutlet weak var speedViewHeight: NSLayoutConstraint!//60
    @IBOutlet var speedView: UIView!
    
    private var locationManager = CLLocationManager()
    private var internetTest: InternetSpeedTest?

    
    @IBOutlet weak var VPNConnectBtn: UIButton!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var purchageView: UIView!
    @IBOutlet weak var contryImg: UIImageView!
    @IBOutlet weak var locationName: UILabel!
    
    var vpnManager: NETunnelProviderManager = NETunnelProviderManager()
    
    var manager: NETunnelProviderManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        speedView.layer.cornerRadius = 10
        speedView.clipsToBounds = true
        setupSpeedTest()
        statusLbl.text = "Connect"
        NotificationCenter.default.addObserver(self, selector: #selector(self.VPNStatusDidChange(_:)), name: NSNotification.Name.NEVPNStatusDidChange, object: nil)
        self.VPNStatus()
        
        
        
    }
    func VPNStatus() {
        
        if isVPNConnected() {
           
            
            CheckSpeed()
           
            statusLbl.text = "Connected"

//            speedView.isHidden = false
            
           
        } else {
            speedView.isHidden = true

            statusLbl.text = "Connect"

//            switchOnOffImg.image = #imageLiteral(resourceName: "switch-off")
//            switchBgView.cornerRadius = Double(switchBgView.frame.height / 2)
//            switchBgView.backgroundColor = AppColors.backGroundBlack
//            switchBgView.borderWidth = 2
//            switchBgView.borderColor = AppColors.PinkAppColor
//
//            UserDefaultsManager().isConnectedToVPN = false
           
        }
    }
    func setupSpeedTest() {
        internetTest = InternetSpeedTest(delegate: self)
        
        if CLLocationManager.locationServicesEnabled() {
                    locationManager.delegate = self
                    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
                    locationManager.requestWhenInUseAuthorization()
                    locationManager.requestAlwaysAuthorization()
                    locationManager.startUpdatingLocation()
                }
    }
    override func viewWillAppear(_ animated: Bool) {
        contryImg.image = UIImage(named: ovpnFile)
        locationName.text = "\(ovpnFile)"
                
        if serverSelectFlag && (purchageStatusApp || freeServerSelectFlag == true){
            self.manager?.connection.stopVPNTunnel()
            serverSelectFlag = false
            do {
                try self.manager?.connection.startVPNTunnel()
            } catch {
                print("second if \(error)")
            }
        }
        
        if purchageStatusApp{
            purchageView.isHidden = true
        }
    }
    
    func VPNStatusDidChange() {
        print("VPN Status changed:")
        let status = self.manager?.connection.status
        switch status {
        case .connecting:
            print("Connecting...")
            statusLbl.text = "Connecting"
            //connectButton.setTitle("Disconnect", for: .normal)
            break
        case .connected:
            print("Connected...")
            statusLbl.text = "Connected"
            VPNStatus()

            //connectButton.setTitle("Disconnect", for: .normal)
            break
        case .disconnecting:
            print("Disconnecting...")
            statusLbl.text = "Disconnecting"
            break
        case .disconnected:
            DispatchQueue.main.async {
                self.speedView.isHidden = true
            }
            print("Disconnected...")
            statusLbl.text = "Connect"
            //connectButton.setTitle("Connect", for: .normal)
            break
        case .invalid:
            print("Invliad")
            break
        case .reasserting:
            print("Reasserting...")
            break
        case .none:
            print("none")
        case .some(_):
            print("some")
        }
    }
    
    @objc func VPNStatusDidChange(_ notification: Notification?) {
        print("VPN Status changed:")
        let status = self.manager?.connection.status
        switch status {
        case .connecting:
            print("Connecting...")
            statusLbl.text = "Connecting"
            //connectButton.setTitle("Disconnect", for: .normal)
            break
        case .connected:
            print("Connected...")
            statusLbl.text = "Connected"
            DispatchQueue.main.async {
                self.speedView.isHidden = true
            }
            VPNStatus()
            //connectButton.setTitle("Disconnect", for: .normal)
            break
        case .disconnecting:
            print("Disconnecting...")
            statusLbl.text = "Disconnecting"
            break
        case .disconnected:
            print("Disconnected...")
            statusLbl.text = "Connect"
            //connectButton.setTitle("Connect", for: .normal)
            break
        case .invalid:
            print("Invliad")
            break
        case .reasserting:
            print("Reasserting...")
            break
        case .none:
            print("none")
        case .some(_):
            print("some")
        }
    }
    
    @IBAction func accountBtnTap(_ sender: Any) {
        let accountVc = self.storyboard?.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
        
        self.navigationController?.pushViewController(accountVc, animated: true)
    }
    @IBAction func menuBtnTap(_ sender: Any) {
       let accountVc = self.storyboard?.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
       
       self.navigationController?.pushViewController(accountVc, animated: true)
        
        //self.navigationController?.popViewController(animated: true)
    }
    @IBAction func privateBrowserBtn(_ sender: Any) {
        
        let subscriptionVc = self.storyboard?.instantiateViewController(withIdentifier: "PrivateBrowserVC") as! PrivateBrowserVC
        
        self.navigationController?.pushViewController(subscriptionVc, animated: true)
    }
    @IBAction func VPNConnectBtnTap(_ sender: Any) {
        print("Go!")
        if purchageStatusApp || freeServerSelectFlag == true {
            let callback = { (error: Error?) -> Void in
                self.manager?.loadFromPreferences(completionHandler: { (error) in
                    guard error == nil else {
                        print("\(error!.localizedDescription)")
                        return
                    }
                    
//                    let options: [String : NSObject] = [
//                        "username": username as NSString,
//                        "password": pass as NSString
//                    ]
                    let options: [String : NSObject] = [
                        "username": "" as NSString,
                        "password": "" as NSString
                    ]
                    
                    if (self.statusLbl.text == "Connect") {
                        do {
                            try self.manager?.connection.startVPNTunnel(options: options)
                        } catch {
                            print("\(error.localizedDescription)")
                        }
                    }else{
                        self.manager?.connection.stopVPNTunnel()
                    }
                })
            }
            
            configureVPN(callback: callback)
        }else{
            let subscriptionVc = self.storyboard?.instantiateViewController(withIdentifier: "SubscribeVC") as! SubscribeVC
            
            self.navigationController?.pushViewController(subscriptionVc, animated: true)
        }
    }
    
    @IBAction func serverBtnTap(_ sender: Any) {
        let serverVc = self.storyboard?.instantiateViewController(withIdentifier: "ServerVC") as! ServerVC
        self.navigationController?.pushViewController(serverVc, animated: true)
    }
    
    @IBAction func premiumUnlockBtnTap(_ sender: Any) {
        let subscriptionVc = self.storyboard?.instantiateViewController(withIdentifier: "SubscribeVC") as! SubscribeVC
        
        self.navigationController?.pushViewController(subscriptionVc, animated: true)
    }
}

extension HomeVC {
    
    func configureVPN(callback: @escaping (Error?) -> Void) {
        print("ovpnFile \(ovpnFile)")
        let configurationFile = Bundle.main.url(forResource: ovpnFile, withExtension: "ovpn")
        let configurationContent = try! Data(contentsOf: configurationFile!)
        print("configurationFile \(configurationFile)")
        print("configurationContent \(configurationContent)")
        
//        guard
//            let configurationFile = Bundle.main.url(forResource: ovpnFile, withExtension: "ovpn"),
//            let configurationContent = try? Data(contentsOf: configurationFile)
//        else {
//
////            UserDefaultsManager().isConnectedToVPN = false
//            print("Disconnected..")
//
//            fatalError()
//        }
      
        
        NETunnelProviderManager.loadAllFromPreferences { (managers, error) in
            guard error == nil else {
                print("\(error!.localizedDescription)")
                callback(error)
                return
            }
            
            self.manager = managers?.first ?? NETunnelProviderManager()
            self.manager?.loadFromPreferences(completionHandler: { (error) in
                guard error == nil else {
                    print("\(error!.localizedDescription)")
                    callback(error)
                    return
                }
                
                let tunnelProtocol = NETunnelProviderProtocol()
                
                if #available(iOS 14.0, *) {
                    tunnelProtocol.includeAllNetworks = true
                } else {
                    // Fallback on earlier versions
                }
                
                tunnelProtocol.serverAddress = ""
//                tunnelProtocol.serverAddress = serverAddress
                tunnelProtocol.providerBundleIdentifier = targetBundleId
//                tunnelProtocol.providerConfiguration = ["configuration": configurationContent]
                tunnelProtocol.providerConfiguration = ["ovpn":configurationContent]
                tunnelProtocol.disconnectOnSleep = false
                
                self.manager?.protocolConfiguration = tunnelProtocol
                self.manager?.localizedDescription = "secureVPN"
                
                self.manager?.isEnabled = true
                self.manager?.saveToPreferences(completionHandler: { (error) in
                    guard error == nil else {
                        print("\(error!.localizedDescription)")
                        callback(error)
                        return
                    }
                    
                    callback(nil)
                })
            })
        }
    }
}
extension HomeVC:CLLocationManagerDelegate {
    
    func isVPNConnected() -> Bool {
        let cfDict = CFNetworkCopySystemProxySettings()
        let nsDict = cfDict!.takeRetainedValue() as NSDictionary
        
        if nsDict.allKeys.count > 0 {
            let keys = nsDict["__SCOPED__"] as! NSDictionary
            for key: String in keys.allKeys as! [String] {
                if (key == "tap" || key == "utun2" || key == "tun" || key == "ppp" || key == "ipsec" || key == "ipsec0" || key.contains("utun")) {
                    return true
                }
            }
        } else {
            return false
        }
        
        return false
    }

    
    func CheckSpeed() {
        print("in speed checker")
        if isVPNConnected() {
            print("in if  speed checker")
            internetTest = InternetSpeedTest(delegate: self)
            
                    internetTest?.startTest() { (error) in
                        
                    }
            
        } else {
            print("in else  speed checker")
            showAlet(message: "Turn on vpn first", Title: "Warnning!", controller: self) { status in
                
            }
            
        }
    }

    
    func internetTest(finish error: SpeedTestError) {
           print(error)
       }
       
       func internetTest(finish result: SpeedTestResult) {
        DispatchQueue.main.async {
            
//            AppDelegate.sharedAppDelegate.hideActivity()
//            self.uploadSpeedLbl.text = "\(result.uploadSpeed.mbps.rounded())"
//            self.downloadSpeedLbl.text = "\(result.downloadSpeed.mbps.rounded())"
           }
        
           print(result.downloadSpeed.mbps)
           print(result.uploadSpeed.mbps)
           print(result.latencyInMs)
           
       }
    
    func internetTest(received servers: [SpeedTestServer]) {
        
    }
    
    func internetTest(selected server: SpeedTestServer, latency: Int) {
        
    }
    
    func internetTestDownloadStart() {
//        DispatchQueue.main.async {
//            self.downloadIcon.startShimmeringAnimation(animationSpeed: 1.5, direction: .topToBottom, repeatCount: MAXFLOAT,lightColor: UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.1).cgColor,darkColor: UIColor.black.cgColor)
//        }
        print("start download")
        
    }
    
    func internetTestDownloadFinish() {
        print("finsh download")
//        downloadIcon.stopShimmeringAnimation()
//        finishDownloadStats = true
    }
    
   
    
    func internetTestUploadStart() {
        
        print("start upload")
    }
    
    func internetTestUploadFinish() {
        print("finsh upload")
        
//        finishUploadStats = true
    }
    func internetTestError(error: SpeedTestError) {
        print("internetTestError \(error)")
    }
    
    func internetTestFinish(result: SpeedTestResult) {
        print("internetTestFinish")
    }
    
    func internetTestReceived(servers: [SpeedTestServer]) {
        print("internetTestReceived")
        DispatchQueue.main.async {
            self.speedView.isHidden = false
        }
        
    }
    
    func internetTestSelected(server: SpeedTestServer, latency: Int, jitter: Int) {
        print("internetTestSelected")
    }
    
    func internetTestDownload(progress: Double, speed: SpeedTestSpeed) {
        print("start download")
        DispatchQueue.main.async {
            let speedCheck = speed.mbps.rounded()
                self.downloadSpeedLbl.text = "\(speedCheck ?? 0)"
           }

    }
    func internetTestUpload(progress: Double, speed: SpeedTestSpeed) {
        print("internetTestUpload")
                if !finishUploadStats {
                    DispatchQueue.main.async {
                        let speedCheck = speed.mbps.rounded()
                        self.uploadSpeedLbl.text = "\(speedCheck ?? 0)"
                       }
                }
    }
   
    
}
