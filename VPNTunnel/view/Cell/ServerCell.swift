//
//  ServerCell.swift
//  VPNTunnel
//
//  Created by Kartik Jasolia on 12/05/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
//

import UIKit

class ServerCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
