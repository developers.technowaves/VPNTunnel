//
//  ViewController.swift
//  VPNTunnel
//
//  Created by Kartik Jasolia on 11/05/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
//

import UIKit

class TermVC: UIViewController {

    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var txtView: UITextView!
    
    var myMutableString = NSMutableAttributedString()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myMutableString = NSMutableAttributedString(string: appName)
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0), range: NSRange(location: 0, length: 11))
        
        myMutableString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.init(red: 254/255, green: 210/255, blue: 48/255, alpha: 1.0), range: NSRange(location: 11, length: 9))
        titleName.attributedText = myMutableString
        
        firstTime()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.txtView.contentOffset = .zero
    }
    
    func firstTime(){
        if let firstFlag = UserDefaults.standard.bool(forKey: firstTimeFlag) as? Bool{
            if firstFlag{
                serverAddress = UserDefaults.standard.object(forKey: "serverAddress") as? String ?? "138.197.175.208"
                ovpnFile = UserDefaults.standard.object(forKey: "ovpnFile") as? String ?? "Canada"
                username = UserDefaults.standard.object(forKey: "username") as? String ?? "root"
                pass = UserDefaults.standard.object(forKey: "pass") as? String ?? "123@Diya"
                
                freeServerSelectFlag = UserDefaults.standard.object(forKey: "freeServerSelectFlag") as? Bool ?? false
                
                let homeVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                 self.navigationController?.pushViewController(homeVc, animated: true)
            }else{
                setDefaultData()
            }
        }else{
            setDefaultData()
        }
    }
    
    func setDefaultData(){
        UserDefaults.standard.set(serverAddress, forKey: "serverAddress")
        UserDefaults.standard.set(ovpnFile, forKey: "ovpnFile")
        UserDefaults.standard.set(username, forKey: "username")
        UserDefaults.standard.set(pass, forKey: "pass")
        UserDefaults.standard.set(freeServerSelectFlag, forKey: "freeServerSelectFlag")
    }

    @IBAction func policyBtnTap(_ sender: Any) {
        if let url: URL = URL(string: "https://sites.google.com/view/secure-vpnios/home"){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
               // Fallback on earlier versions
               UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func agreeBtnTap(_ sender: Any) {
        let globeVc = self.storyboard?.instantiateViewController(withIdentifier: "GlobeVC") as! GlobeVC
        
       UserDefaults.standard.set(true, forKey: firstTimeFlag)
        self.navigationController?.pushViewController(globeVc, animated: true)
    }
}

