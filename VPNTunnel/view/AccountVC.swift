//
//  AccountVC.swift
//  VPNTunnel
//
//  Created by Kartik Jasolia on 12/05/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
//

import UIKit

class AccountVC: UIViewController {

    
    @IBOutlet weak var accountTypeImg: UIImageView!
    @IBOutlet weak var accountTypeLbl: UILabel!
    @IBOutlet weak var goPremiumView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.noRestore), name: .noRestordData, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .noRestordData, object: nil)
    }
    
   
    @objc func noRestore(){
        let alert = UIAlertController(title: "Subscription Info", message: "you are not Subscribe right now", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    func updateData(){
        if purchageStatusApp {
            accountTypeImg.image = UIImage(named: "profile_VIP")
            accountTypeLbl.text = "VIP Account"
            goPremiumView.isHidden = true
        }else{
            accountTypeImg.image = UIImage(named: "profile_free")
            accountTypeLbl.text = "Free Account"
            goPremiumView.isHidden = false
        }
    }

    func RateUs(){
        if let url = URL(string: "https://apps.apple.com/us/app/id1522664954")
        {
                   if #available(iOS 10.0, *) {
                      UIApplication.shared.open(url, options: [:], completionHandler: nil)
                   }
                   else {
                         if UIApplication.shared.canOpenURL(url as URL) {
                            UIApplication.shared.openURL(url as URL)
                        }
                   }
        }
    }
    
    func Mail(){
        if let url: URL = URL(string: "mailto:approtic@gmail.com"){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
               // Fallback on earlier versions
               UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func goPremiumBtnTap(_ sender: Any) {
        let subscriptionVc = self.storyboard?.instantiateViewController(withIdentifier: "SubscribeVC") as! SubscribeVC
        
        self.navigationController?.pushViewController(subscriptionVc, animated: true)
    }
    
    @IBAction func restoreBtnTap(_ sender: Any) {
        AppDelegate.sharedAppDelegate.showActivityWithMessage(message: "", inView: self.view)
        IPAService.shared.restorePurchases()
    }
    
    @IBAction func feedbackBtnTap(_ sender: Any) {
        Mail()
    }
    
    @IBAction func rateUsBtnTap(_ sender: Any) {
        RateUs()
    }
    
    @IBAction func aboutBtnTap(_ sender: Any) {
        let optionVc = self.storyboard?.instantiateViewController(withIdentifier: "OptionVC") as! OptionVC

        self.navigationController?.pushViewController(optionVc, animated: true)
    }
    
    @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
