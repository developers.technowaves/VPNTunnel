//
//  SubscribeVC.swift
//  VPNTunnel
//
//  Created by Kartik Jasolia on 12/05/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
// First 3 days free

import UIKit

class SubscribeVC: UIViewController {

    @IBOutlet weak var weekBtn: zwView!
    @IBOutlet weak var mounthBtn: zwView!
    @IBOutlet weak var weekbtn: UIButton!
    @IBOutlet weak var monthbtn: UIButton!
    @IBOutlet weak var yearBtn: zwView!
    @IBOutlet weak var purchaseBtn: UIButton!
    @IBOutlet weak var purchaseView: UIView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var weekPriceLbl: UILabel!
    @IBOutlet weak var monthPriceLbl: UILabel!
    @IBOutlet weak var conditionLbl: UILabel!
    @IBOutlet weak var txtView: UITextView!
    
    var subscriptionTag = 0
    
    var GlobeFlag = false
    
    var timer = Timer()
    
    let conditionWeekStr = "Free for 3 days,than "
    
    let conditionMonthStr = "Free for 3 days,than "
    
    var weekPrice = "$9.99"
    var monthPrice = "$29.99"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if GlobeFlag{
            cancelBtn.isHidden = false
        }else{
            cancelBtn.isHidden = true
        }
        
        mounthBtn.borderColor = #colorLiteral(red: 0.999720037, green: 0.8484913111, blue: 0.239778161, alpha: 1)
        monthbtn.setImage(UIImage(named: "circle_fill_yellow"), for: .normal)
        weekbtn.setImage(nil, for: .normal)
        subscriptionTag = 2
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkSubscription), name: .purchageNotif, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.noRestore), name: .noRestordData, object: nil)
        
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (_) in
            if finalMonthPrice != "" && finalWeekPrice != ""{
                self.weekPriceLbl.text = finalWeekPrice + "/1 week"
                self.monthPriceLbl.text = finalMonthPrice + "/1 month"
                if self.subscriptionTag == 1{
                    self.conditionLbl.text = self.conditionWeekStr + finalMonthPrice + "/week"
                }else{
                    self.conditionLbl.text = self.conditionMonthStr + finalMonthPrice + "/month"
                }
                self.weekPrice = finalWeekPrice
                self.monthPrice = finalMonthPrice
                self.timer.invalidate()
            }
        }
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0.0, options: [.allowUserInteraction], animations: {
                self.purchaseView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            }) { (_) in
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [.allowUserInteraction], animations: {
                    self.purchaseView.transform = CGAffineTransform.identity
                }, completion: nil)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: .purchageNotif, object: nil)
        NotificationCenter.default.removeObserver(self, name: .noRestordData, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.txtView.contentOffset = .zero
    }
    
    @objc func noRestore(){
        let alert = UIAlertController(title: "Subscription Info", message: "you are not Subscribe right now", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func restoreBtnTap(_ sender: Any) {
        AppDelegate.sharedAppDelegate.showActivityWithMessage(message: "", inView: self.view)
        IPAService.shared.restorePurchases()
    }
        
    @IBAction func weekBtnTap(_ sender: Any) {
        weekBtn.borderColor = #colorLiteral(red: 0.999720037, green: 0.8484913111, blue: 0.239778161, alpha: 1)
        mounthBtn.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        yearBtn.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        weekbtn.setImage(UIImage(named: "circle_fill_yellow"), for: .normal)
        monthbtn.setImage(nil, for: .normal)
        
        self.conditionLbl.text = self.conditionWeekStr + weekPrice + "/week"
        
        subscriptionTag = 1
    }
    
    @IBAction func mounthBtnTap(_ sender: Any) {
        weekBtn.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        mounthBtn.borderColor = #colorLiteral(red: 0.999720037, green: 0.8484913111, blue: 0.239778161, alpha: 1)
        yearBtn.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        monthbtn.setImage(UIImage(named: "circle_fill_yellow"), for: .normal)
        weekbtn.setImage(nil, for: .normal)
        
        self.conditionLbl.text = self.conditionMonthStr + monthPrice + "/month"
        
        subscriptionTag = 2
    }
    
    @IBAction func yearBtnTap(_ sender: Any) {
        weekBtn.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        mounthBtn.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        yearBtn.borderColor = #colorLiteral(red: 0.999720037, green: 0.8484913111, blue: 0.239778161, alpha: 1)
        subscriptionTag = 3
    }
    
    @IBAction func subscriptionBtnTap(_ sender: Any) {
        switch subscriptionTag {
        case 1:
            //week
            AppDelegate.sharedAppDelegate.showActivityWithMessage(message: "Loading...", inView: self.view)
            IPAService.shared.purchase(product: .VPNSubWeek)
            break
        case 2:
            //mounth
            AppDelegate.sharedAppDelegate.showActivityWithMessage(message: "Loading...", inView: self.view)
            IPAService.shared.purchase(product: .VPNSubMounth)
            break
        case 3:
            //year
            AppDelegate.sharedAppDelegate.showActivityWithMessage(message: "Loading...", inView: self.view)
            IPAService.shared.purchase(product: .VPNSubYear)
            break
        default:
            print("please select subcription type")
        }
    }
    
    @objc func checkSubscription(){
        if GlobeFlag{
            if purchageStatusApp{
                let homeVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(homeVc, animated: true)
            }
        }else{
            if purchageStatusApp{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func cancelBtnTap(_ sender: Any) {
        let homeVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        self.navigationController?.pushViewController(homeVc, animated: true)
    }
    
    @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
