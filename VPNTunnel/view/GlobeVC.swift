//
//  GlobeVC.swift
//  VPNTunnel
//
//  Created by Kartik Jasolia on 08/07/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
//

import UIKit

class GlobeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func continueBtnTap(_ sender: Any) {
        let subscribeVc = self.storyboard?.instantiateViewController(withIdentifier: "SubscribeVC") as! SubscribeVC
       
        subscribeVc.GlobeFlag = true
        self.navigationController?.pushViewController(subscribeVc, animated: true)
    }
    
}
