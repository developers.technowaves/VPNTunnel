//
//  OptionVC.swift
//  VPNTunnel
//
//  Created by Kartik Jasolia on 12/05/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
//

import UIKit

class OptionVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func Mail(){
        if let url: URL = URL(string: "mailto:approtic@gmail.com"){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
               // Fallback on earlier versions
               UIApplication.shared.openURL(url)
            }
        }
    }
    
    func policy_Terms(){
        if let url: URL = URL(string: "https://sites.google.com/view/secure-vpnios/home"){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
               // Fallback on earlier versions
               UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func termOfServiceBtnTap(_ sender: Any) {
//        let termPolicyVc = self.storyboard?.instantiateViewController(withIdentifier: "TermPolicyVC") as! TermPolicyVC
//
//        termPolicyVc.titleStr = "Terms Of Service"
//        self.navigationController?.pushViewController(termPolicyVc, animated: true)
        policy_Terms()
    }
    
    @IBAction func privacyPolicyBtnTap(_ sender: Any) {
        
//        let termPolicyVc = self.storyboard?.instantiateViewController(withIdentifier: "TermPolicyVC") as! TermPolicyVC
//
//        termPolicyVc.titleStr = "Privacy Policy"
//        self.navigationController?.pushViewController(termPolicyVc, animated: true)
        policy_Terms()
    }
    
    @IBAction func
        AboutUsBtnTap(_ sender: Any) {
        if let url: URL = URL(string: "https://sites.google.com/view/secure-vpnios/about-us"){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
               // Fallback on earlier versions
               UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func helpBtnTap(_ sender: Any) {
        Mail()
    }
    
    @IBAction func backBtnTap(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
