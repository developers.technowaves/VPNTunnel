//
//  AppDelegate.swift
//  VPNTunnel
//
//  Created by Kartik Jasolia on 11/05/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
//

import UIKit
import IQKeyboardManager
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let sharedAppDelegate = AppDelegate()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        IQKeyboardManager.shared().isEnabled = true

//        UserDefaults.standard.set(true, forKey: purchaseStatus)
        IPAService.shared.getProduct()
        //IPAService.shared.validateReceipt()
        if let status = UserDefaults.standard.bool(forKey: purchaseStatus) as? Bool{
            if status{
                purchageStatusApp = true
                addcontroller()
            }else{
                purchageStatusApp = false
            }
        }
        
        return true
    }
    
    
    func addcontroller(){
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeVC: UIViewController? = storyboard.instantiateViewController(withIdentifier: "HomeVC") as? HomeVC
        let homeNavi = UINavigationController(rootViewController: homeVC!)
        homeNavi.navigationBar.isHidden = true
        homeNavi.interactivePopGestureRecognizer?.isEnabled = false
        self.window?.rootViewController = homeNavi
        self.window?.makeKeyAndVisible()
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


    
    var blackView: UIView? = nil
        open func showActivityWithMessage(message: String, inView view: UIView) {
            if blackView == nil {
            blackView = UIView(frame: UIScreen.main.bounds)
            blackView!.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            var factor: Float = 1.0
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                factor = 2.0
            }
            
            var rect = CGRect.init()
            rect.size.width = CGFloat(factor * Float(200))
            rect.size.height = CGFloat(factor * Float(70))
            rect.origin.y = CGFloat((blackView?.frame.size.height)! - rect.size.height) / CGFloat(2.0)
            rect.origin.x = CGFloat((blackView?.frame.size.width)! - rect.size.width) / CGFloat(2.0)
            
            let waitingView = UIView(frame: rect)
            waitingView.backgroundColor = UIColor.clear
            waitingView.layer.cornerRadius = 8.0
            
            rect = waitingView.bounds;
            rect.size.height = CGFloat(Float(40.0) * factor);
            
            let label = UILabel(frame: rect)
            label.text = message
            label.textColor = UIColor.white
            label.textAlignment = .center
                label.font = UIFont.systemFont(ofSize: CGFloat(Float(16.0) * factor))//UIFont(name: APP_BOLD_FONT, size: CGFloat(Float(16.0) * factor))
            waitingView.addSubview(label)
            
                let activityIndicatorView = UIActivityIndicatorView.init(style: .whiteLarge)
            activityIndicatorView.color = UIColor.white
            
            rect = activityIndicatorView.frame;
            rect.origin.x = ((waitingView.frame.size.width - rect.size.width)/2.0);
            rect.origin.y = label.frame.size.height + 3.0;
            activityIndicatorView.frame = rect;
            
            waitingView.addSubview(activityIndicatorView)
            activityIndicatorView.startAnimating()
            
            blackView?.tag = 1010
            blackView?.addSubview(waitingView)
            view.addSubview(blackView!)
            }
        }
        
        func hideActivity() {
            blackView?.removeFromSuperview()
            blackView = nil
        }
}

