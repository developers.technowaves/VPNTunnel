##############################################
# Sample client-side OpenVPN 2.0 config file #
# for connecting to multi-client server.     #
#                                            #
# This configuration can be used by multiple #
# clients, however each client should have   #
# its own cert and key files.                #
#                                            #
# On Windows, you might want to rename this  #
# file so it has a .ovpn extension           #
##############################################

# Specify that we are a client and that we
# will be pulling certain config file directives
# from the server.
client

# Use the same setting as you are using on
# the server.
# On most systems, the VPN will not function
# unless you partially or fully disable
# the firewall for the TUN/TAP interface.
;dev tap
dev tun

# Windows needs the TAP-Win32 adapter name
# from the Network Connections panel
# if you have more than one.  On XP SP2,
# you may need to disable the firewall
# for the TAP adapter.
;dev-node MyTap

# Are we connecting to a TCP or
# UDP server?  Use the same setting as
# on the server.
;proto tcp
proto udp

# The hostname/IP and port of the server.
# You can have multiple remote entries
# to load balance between the servers.
remote 159.65.28.168 1194
;remote my-server-2 1194

# Choose a random host from the remote
# list for load-balancing.  Otherwise
# try hosts in the order specified.
;remote-random

# Keep trying indefinitely to resolve the
# host name of the OpenVPN server.  Very useful
# on machines which are not permanently connected
# to the internet such as laptops.
resolv-retry infinite

# Most clients don't need to bind to
# a specific local port number.
nobind

# Downgrade privileges after initialization (non-Windows only)
user nobody
group nogroup

# Try to preserve some state across restarts.
persist-key
persist-tun

# If you are connecting through an
# HTTP proxy to reach the actual OpenVPN
# server, put the proxy server/IP and
# port number here.  See the man page
# if your proxy server requires
# authentication.
;http-proxy-retry # retry on connection failures
;http-proxy [proxy server] [proxy port #]

# Wireless networks often produce a lot
# of duplicate packets.  Set this flag
# to silence duplicate packet warnings.
;mute-replay-warnings

# SSL/TLS parms.
# See the server config file for more
# description.  It's best to use
# a separate .crt/.key file pair
# for each client.  A single ca
# file can be used for all clients.
;ca ca.crt
;cert client.crt
;key client.key

# Verify server certificate by checking that the
# certicate has the correct key usage set.
# This is an important precaution to protect against
# a potential attack discussed here:
#  http://openvpn.net/howto.html#mitm
#
# To use this feature, you will need to generate
# your server certificates with the keyUsage set to
#   digitalSignature, keyEncipherment
# and the extendedKeyUsage to
#   serverAuth
# EasyRSA can do this for you.
remote-cert-tls server

# If a tls-auth key is used on the server
# then every client must also have the key.
;tls-auth ta.key 1

# Select a cryptographic cipher.
# If the cipher option is used on the server
# then you must also specify it here.
# Note that v2.4 client/server will automatically
# negotiate AES-256-GCM in TLS mode.
# See also the ncp-cipher option in the manpage
;cipher AES-256-CBC
cipher AES-256-GCM
auth SHA256

# Enable compression on the VPN link.
# Don't enable this unless it is also
# enabled in the server config file.
#comp-lzo

# Set log file verbosity.
verb 3

# Silence repeating messages
;mute 20

key-direction 1

; script-security 2
; up /etc/openvpn/update-resolv-conf
; down /etc/openvpn/update-resolv-conf

; script-security 2
; up /etc/openvpn/update-systemd-resolved
; down /etc/openvpn/update-systemd-resolved
; down-pre
; dhcp-option DOMAIN-ROUTE .

<ca>
-----BEGIN CERTIFICATE-----
MIIB/DCCAYKgAwIBAgIUYmAo58OxxODW12jQLH5jQYaspfAwCgYIKoZIzj0EAwQw
FjEUMBIGA1UEAwwLRWFzeS1SU0EgQ0EwHhcNMjEwNzIzMDQyNzQ2WhcNMzEwNzIx
MDQyNzQ2WjAWMRQwEgYDVQQDDAtFYXN5LVJTQSBDQTB2MBAGByqGSM49AgEGBSuB
BAAiA2IABItRg6w1XprVTa8SREXF9PmsOYcl1zCvg5G0nFdoYbNU3obHCPfHxnjk
qLN7J1vcASuD3trNTpMzS4VZO+SPjHCYZc3IrvIA3Vkkb7uJI4rJFYGLQTW5Cf/f
GJr/i8Y8n6OBkDCBjTAdBgNVHQ4EFgQUNS8V7iGgnBEXEvY0PIRjsquHDkQwUQYD
VR0jBEowSIAUNS8V7iGgnBEXEvY0PIRjsquHDkShGqQYMBYxFDASBgNVBAMMC0Vh
c3ktUlNBIENBghRiYCjnw7HE4NbXaNAsfmNBhqyl8DAMBgNVHRMEBTADAQH/MAsG
A1UdDwQEAwIBBjAKBggqhkjOPQQDBANoADBlAjEAlhZcRuAAH27G1yU+jbK1Nvoj
MstL7gcVHMY0Xoe/hm2UvKo5M5f2C4w7B2xiCxLkAjBGYVJuEB7VXSBPYK5qIBsb
JOWSg8YJfG+e92+pKt0TiSvuz6OP6NpLO2fmycqmH68=
-----END CERTIFICATE-----
</ca>
<cert>
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            01:b7:36:34:60:c1:de:88:47:0f:3c:13:ce:dc:55:44
        Signature Algorithm: ecdsa-with-SHA512
        Issuer: CN=Easy-RSA CA
        Validity
            Not Before: Jul 23 06:54:36 2021 GMT
            Not After : Jul  7 06:54:36 2024 GMT
        Subject: CN=cldn
        Subject Public Key Info:
            Public Key Algorithm: id-ecPublicKey
                Public-Key: (384 bit)
                pub:
                    04:7d:58:8c:51:a6:51:e0:a4:11:1b:c5:1b:c1:93:
                    40:05:9e:e2:5a:6a:9c:da:74:e2:9a:49:d9:96:6e:
                    10:47:2d:3c:16:da:6b:8e:5f:89:33:a6:74:b8:ce:
                    c5:6f:cb:a1:ab:d2:18:70:a0:ae:46:d0:a5:48:19:
                    51:73:42:2b:16:dd:b7:bf:4b:86:82:14:19:64:0f:
                    79:ea:5d:a7:6b:2f:73:5f:12:80:6e:85:fc:35:c6:
                    91:77:8e:ed:3e:cd:66
                ASN1 OID: secp384r1
                NIST CURVE: P-384
        X509v3 extensions:
            X509v3 Basic Constraints:
                CA:FALSE
            X509v3 Subject Key Identifier:
                77:9D:4A:07:27:B9:59:F2:1A:F6:A0:E4:EF:B8:7F:A8:9C:1B:E9:7A
            X509v3 Authority Key Identifier:
                keyid:35:2F:15:EE:21:A0:9C:11:17:12:F6:34:3C:84:63:B2:AB:87:0E:44
                DirName:/CN=Easy-RSA CA
                serial:62:60:28:E7:C3:B1:C4:E0:D6:D7:68:D0:2C:7E:63:41:86:AC:A5:F0

            X509v3 Extended Key Usage:
                TLS Web Client Authentication
            X509v3 Key Usage:
                Digital Signature
    Signature Algorithm: ecdsa-with-SHA512
         30:65:02:31:00:94:f2:d6:c7:89:29:67:fd:df:59:05:b3:bc:
         97:65:ea:5c:57:76:0a:26:e2:e0:81:cd:eb:15:2b:cc:9f:5c:
         dc:7f:69:4d:3e:e5:d5:b7:2e:00:33:fb:28:40:bd:c9:d1:02:
         30:24:00:79:52:17:6f:16:3d:f9:b2:c7:3f:ac:ce:9b:3a:78:
         da:d7:95:6a:3b:b2:15:66:b4:95:2f:03:4a:98:78:01:a7:01:
         bb:c9:82:10:56:b1:04:93:1a:e9:44:41:0a
-----BEGIN CERTIFICATE-----
MIICAzCCAYmgAwIBAgIQAbc2NGDB3ohHDzwTztxVRDAKBggqhkjOPQQDBDAWMRQw
EgYDVQQDDAtFYXN5LVJTQSBDQTAeFw0yMTA3MjMwNjU0MzZaFw0yNDA3MDcwNjU0
MzZaMA8xDTALBgNVBAMMBGNsZG4wdjAQBgcqhkjOPQIBBgUrgQQAIgNiAAR9WIxR
plHgpBEbxRvBk0AFnuJaapzadOKaSdmWbhBHLTwW2muOX4kzpnS4zsVvy6Gr0hhw
oK5G0KVIGVFzQisW3be/S4aCFBlkD3nqXadrL3NfEoBuhfw1xpF3ju0+zWajgaIw
gZ8wCQYDVR0TBAIwADAdBgNVHQ4EFgQUd51KBye5WfIa9qDk77h/qJwb6XowUQYD
VR0jBEowSIAUNS8V7iGgnBEXEvY0PIRjsquHDkShGqQYMBYxFDASBgNVBAMMC0Vh
c3ktUlNBIENBghRiYCjnw7HE4NbXaNAsfmNBhqyl8DATBgNVHSUEDDAKBggrBgEF
BQcDAjALBgNVHQ8EBAMCB4AwCgYIKoZIzj0EAwQDaAAwZQIxAJTy1seJKWf931kF
s7yXZepcV3YKJuLggc3rFSvMn1zcf2lNPuXVty4AM/soQL3J0QIwJAB5UhdvFj35
ssc/rM6bOnja15VqO7IVZrSVLwNKmHgBpwG7yYIQVrEEkxrpREEK
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIG2AgEAMBAGByqGSM49AgEGBSuBBAAiBIGeMIGbAgEBBDDezw7dtrarbLmTES8O
a13s3FAaO1vLSHanGfonXmXLhibTJxWtfNh4KD/RLUszwSihZANiAAR9WIxRplHg
pBEbxRvBk0AFnuJaapzadOKaSdmWbhBHLTwW2muOX4kzpnS4zsVvy6Gr0hhwoK5G
0KVIGVFzQisW3be/S4aCFBlkD3nqXadrL3NfEoBuhfw1xpF3ju0+zWY=
-----END PRIVATE KEY-----
</key>
<tls-crypt>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
224141b8480ba9654efdad316b54248e
e49c4648d1805e7b213f6cac27aa6610
137bb1392f03502d7a1400ab0b72f085
2ad019e8d34e6c9f2af8d4572159c338
4f388234de23064dfeee288a23a33aa3
f6f3220577840a3567c2f9b4a1261fe6
3c63d55a46aeb11d6c26347ee7a8893a
d98eaedef680a912ecf8d66c4158bf91
e4acbe00235cf76e541afa53a9a081e4
93d999abf54b4fe2cc7e010cf57c4fd1
dedb4d39dae3961289c10c6ee9f7f5d7
4cf4d96d1cef0bc632ee5a7513ab9400
ca1917f7f007ea89766dbbdbd973939b
01dacf77334acffaa863e1aa311dc0f2
89a981ad42ee9461ff42f98e5890e07d
90cfca4c03a375f333be41dd062d7159
-----END OpenVPN Static key V1-----
</tls-crypt>
