##############################################
# Sample client-side OpenVPN 2.0 config file #
# for connecting to multi-client server.     #
#                                            #
# This configuration can be used by multiple #
# clients, however each client should have   #
# its own cert and key files.                #
#                                            #
# On Windows, you might want to rename this  #
# file so it has a .ovpn extension           #
##############################################

# Specify that we are a client and that we
# will be pulling certain config file directives
# from the server.
client

# Use the same setting as you are using on
# the server.
# On most systems, the VPN will not function
# unless you partially or fully disable
# the firewall for the TUN/TAP interface.
;dev tap
dev tun

# Windows needs the TAP-Win32 adapter name
# from the Network Connections panel
# if you have more than one.  On XP SP2,
# you may need to disable the firewall
# for the TAP adapter.
;dev-node MyTap

# Are we connecting to a TCP or
# UDP server?  Use the same setting as
# on the server.
;proto tcp
proto udp

# The hostname/IP and port of the server.
# You can have multiple remote entries
# to load balance between the servers.
remote 165.227.44.130 1194
;remote my-server-2 1194

# Choose a random host from the remote
# list for load-balancing.  Otherwise
# try hosts in the order specified.
;remote-random

# Keep trying indefinitely to resolve the
# host name of the OpenVPN server.  Very useful
# on machines which are not permanently connected
# to the internet such as laptops.
resolv-retry infinite

# Most clients don't need to bind to
# a specific local port number.
nobind

# Downgrade privileges after initialization (non-Windows only)
user nobody
group nogroup

# Try to preserve some state across restarts.
persist-key
persist-tun

# If you are connecting through an
# HTTP proxy to reach the actual OpenVPN
# server, put the proxy server/IP and
# port number here.  See the man page
# if your proxy server requires
# authentication.
;http-proxy-retry # retry on connection failures
;http-proxy [proxy server] [proxy port #]

# Wireless networks often produce a lot
# of duplicate packets.  Set this flag
# to silence duplicate packet warnings.
;mute-replay-warnings

# SSL/TLS parms.
# See the server config file for more
# description.  It's best to use
# a separate .crt/.key file pair
# for each client.  A single ca
# file can be used for all clients.
;ca ca.crt
;cert client.crt
;key client.key

# Verify server certificate by checking that the
# certicate has the correct key usage set.
# This is an important precaution to protect against
# a potential attack discussed here:
#  http://openvpn.net/howto.html#mitm
#
# To use this feature, you will need to generate
# your server certificates with the keyUsage set to
#   digitalSignature, keyEncipherment
# and the extendedKeyUsage to
#   serverAuth
# EasyRSA can do this for you.
remote-cert-tls server

# If a tls-auth key is used on the server
# then every client must also have the key.
;tls-auth ta.key 1

# Select a cryptographic cipher.
# If the cipher option is used on the server
# then you must also specify it here.
# Note that v2.4 client/server will automatically
# negotiate AES-256-GCM in TLS mode.
# See also the ncp-cipher option in the manpage
;cipher AES-256-CBC
cipher AES-256-GCM
auth SHA256

# Enable compression on the VPN link.
# Don't enable this unless it is also
# enabled in the server config file.
#comp-lzo

# Set log file verbosity.
verb 3

# Silence repeating messages
;mute 20

key-direction 1

; script-security 2
; up /etc/openvpn/update-resolv-conf
; down /etc/openvpn/update-resolv-conf

; script-security 2
; up /etc/openvpn/update-systemd-resolved
; down /etc/openvpn/update-systemd-resolved
; down-pre
; dhcp-option DOMAIN-ROUTE .

<ca>
-----BEGIN CERTIFICATE-----
MIIB/DCCAYKgAwIBAgIUYmAo58OxxODW12jQLH5jQYaspfAwCgYIKoZIzj0EAwQw
FjEUMBIGA1UEAwwLRWFzeS1SU0EgQ0EwHhcNMjEwNzIzMDQyNzQ2WhcNMzEwNzIx
MDQyNzQ2WjAWMRQwEgYDVQQDDAtFYXN5LVJTQSBDQTB2MBAGByqGSM49AgEGBSuB
BAAiA2IABItRg6w1XprVTa8SREXF9PmsOYcl1zCvg5G0nFdoYbNU3obHCPfHxnjk
qLN7J1vcASuD3trNTpMzS4VZO+SPjHCYZc3IrvIA3Vkkb7uJI4rJFYGLQTW5Cf/f
GJr/i8Y8n6OBkDCBjTAdBgNVHQ4EFgQUNS8V7iGgnBEXEvY0PIRjsquHDkQwUQYD
VR0jBEowSIAUNS8V7iGgnBEXEvY0PIRjsquHDkShGqQYMBYxFDASBgNVBAMMC0Vh
c3ktUlNBIENBghRiYCjnw7HE4NbXaNAsfmNBhqyl8DAMBgNVHRMEBTADAQH/MAsG
A1UdDwQEAwIBBjAKBggqhkjOPQQDBANoADBlAjEAlhZcRuAAH27G1yU+jbK1Nvoj
MstL7gcVHMY0Xoe/hm2UvKo5M5f2C4w7B2xiCxLkAjBGYVJuEB7VXSBPYK5qIBsb
JOWSg8YJfG+e92+pKt0TiSvuz6OP6NpLO2fmycqmH68=
-----END CERTIFICATE-----
</ca>
<cert>
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            10:fc:6b:00:89:87:f7:e6:2a:45:e9:ea:14:88:4d:f1
        Signature Algorithm: ecdsa-with-SHA512
        Issuer: CN=Easy-RSA CA
        Validity
            Not Before: Jul 23 07:37:57 2021 GMT
            Not After : Jul  7 07:37:57 2024 GMT
        Subject: CN=ctrt
        Subject Public Key Info:
            Public Key Algorithm: id-ecPublicKey
                Public-Key: (384 bit)
                pub:
                    04:46:24:6f:88:d2:7c:95:7e:98:12:13:18:c9:da:
                    f8:e2:72:fc:e2:de:69:95:51:25:40:17:e0:b4:de:
                    23:e3:4a:98:40:55:df:5c:d9:ae:66:36:b9:7c:1d:
                    78:37:14:40:d0:4b:90:ca:d8:d6:cd:55:82:12:83:
                    42:81:c5:95:e3:85:e7:4d:4b:4d:da:c7:8f:b4:2b:
                    8c:57:b0:c9:77:93:65:cb:c0:3d:4f:a6:c5:2b:d9:
                    92:ef:26:35:b0:59:c3
                ASN1 OID: secp384r1
                NIST CURVE: P-384
        X509v3 extensions:
            X509v3 Basic Constraints:
                CA:FALSE
            X509v3 Subject Key Identifier:
                C6:AA:84:BC:17:7E:66:0A:23:43:48:59:7C:21:E1:85:FA:E6:C6:ED
            X509v3 Authority Key Identifier:
                keyid:35:2F:15:EE:21:A0:9C:11:17:12:F6:34:3C:84:63:B2:AB:87:0E:44
                DirName:/CN=Easy-RSA CA
                serial:62:60:28:E7:C3:B1:C4:E0:D6:D7:68:D0:2C:7E:63:41:86:AC:A5:F0

            X509v3 Extended Key Usage:
                TLS Web Client Authentication
            X509v3 Key Usage:
                Digital Signature
    Signature Algorithm: ecdsa-with-SHA512
         30:64:02:30:38:8b:77:dc:70:ef:98:00:5b:ff:34:63:8a:4f:
         a4:79:28:61:21:31:bb:d9:16:51:6a:71:d7:24:2e:a4:63:50:
         cc:ad:6f:4c:9f:ef:36:62:39:cb:3e:69:69:df:82:12:02:30:
         71:63:95:cb:a4:95:e4:75:9e:df:6d:5d:e4:e1:e5:5c:eb:59:
         7c:fa:9b:5f:11:0b:eb:60:ab:b6:43:fe:ea:de:28:c6:ef:6a:
         e2:6a:97:d3:97:0b:bd:29:4d:39:cd:e7
-----BEGIN CERTIFICATE-----
MIICAjCCAYmgAwIBAgIQEPxrAImH9+YqRenqFIhN8TAKBggqhkjOPQQDBDAWMRQw
EgYDVQQDDAtFYXN5LVJTQSBDQTAeFw0yMTA3MjMwNzM3NTdaFw0yNDA3MDcwNzM3
NTdaMA8xDTALBgNVBAMMBGN0cnQwdjAQBgcqhkjOPQIBBgUrgQQAIgNiAARGJG+I
0nyVfpgSExjJ2vjicvzi3mmVUSVAF+C03iPjSphAVd9c2a5mNrl8HXg3FEDQS5DK
2NbNVYISg0KBxZXjhedNS03ax4+0K4xXsMl3k2XLwD1PpsUr2ZLvJjWwWcOjgaIw
gZ8wCQYDVR0TBAIwADAdBgNVHQ4EFgQUxqqEvBd+ZgojQ0hZfCHhhfrmxu0wUQYD
VR0jBEowSIAUNS8V7iGgnBEXEvY0PIRjsquHDkShGqQYMBYxFDASBgNVBAMMC0Vh
c3ktUlNBIENBghRiYCjnw7HE4NbXaNAsfmNBhqyl8DATBgNVHSUEDDAKBggrBgEF
BQcDAjALBgNVHQ8EBAMCB4AwCgYIKoZIzj0EAwQDZwAwZAIwOIt33HDvmABb/zRj
ik+keShhITG72RZRanHXJC6kY1DMrW9Mn+82YjnLPmlp34ISAjBxY5XLpJXkdZ7f
bV3k4eVc61l8+ptfEQvrYKu2Q/7q3ijG72riapfTlwu9KU05zec=
-----END CERTIFICATE-----
</cert>
<key>
-----BEGIN PRIVATE KEY-----
MIG2AgEAMBAGByqGSM49AgEGBSuBBAAiBIGeMIGbAgEBBDDMotx7478/04IVw+It
aqhzwuVrM9pbWE0SddmuUQ0lSLRH5b6gYpidclpjwBdXjHehZANiAARGJG+I0nyV
fpgSExjJ2vjicvzi3mmVUSVAF+C03iPjSphAVd9c2a5mNrl8HXg3FEDQS5DK2NbN
VYISg0KBxZXjhedNS03ax4+0K4xXsMl3k2XLwD1PpsUr2ZLvJjWwWcM=
-----END PRIVATE KEY-----
</key>
<tls-crypt>
#
# 2048 bit OpenVPN static key
#
-----BEGIN OpenVPN Static key V1-----
277919932ed2dba4ad9064ed9a0e3a9b
9a568ba8e235cbc3f0c5c3c2e3b355f5
af55e1cd6f69f1ee3a2b83f446845de0
a545f14d0de48bb78c10df18556bfdab
6245c2ecbd085172095e6e85bcb45481
00cf75217ebdd4be5f5291a12c2c4708
62fae20adcb91b942f7f0f1571b95486
d9e2cbcf0d79b3d1ecb77f9bb786f4f0
2d5c04116920bd3b5f2a44684a4179af
10c6ee02948e60c654a6a8e5a1fc3853
d8ee0c32cf1c15a11c7db5215814ee24
d6e2a73e08f4f15848dfd0ca68975a58
c630fcca8a0e85a644edaf0137a5a0da
5d36be3f708c684e09233f00492b55ee
78a2823a87333da424a99644c0939f4b
af2ee8e438822086df05cc78bd803876
-----END OpenVPN Static key V1-----
</tls-crypt>
