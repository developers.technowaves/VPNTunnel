//
//  locoClassInApp.swift
//  instaImgGrid
//
//  Created by Kartik Jasolia on 15/04/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
//

import Foundation
import StoreKit

enum IPAProduct: String{
    case VPNSubWeek = "com.approtic.secureVPN.week"
    case VPNSubMounth  = "com.approtic.secureVPN.mounth"
    case VPNSubYear = "com.approtic.secureVPN.year"
}

class IPAService: NSObject {
    
    private override init() {}
    static let shared = IPAService()
    
    var products = [SKProduct]()
    let paymentQueue = SKPaymentQueue.default()
    
    func getProduct(){
        let products: Set = [
            IPAProduct.VPNSubWeek.rawValue,
            IPAProduct.VPNSubMounth.rawValue,
            IPAProduct.VPNSubYear.rawValue
            ]
        
        let request = SKProductsRequest(productIdentifiers: products)
        request.delegate = self
        request.start()
        paymentQueue.add(self)
    }
    
    func purchase(product: IPAProduct){
        guard let productToPurchase = products.filter({
            $0.productIdentifier == product.rawValue
        }).first else{ return }
        let payment = SKPayment(product: productToPurchase)
        paymentQueue.add(payment)
    }
    
    func restorePurchases(){
        paymentQueue.add(self)
        paymentQueue.restoreCompletedTransactions()
    }
    
    func validateReceipt(){
        #if DEBUG
                   let urlString = "https://sandbox.itunes.apple.com/verifyReceipt"
               #else
                   let urlString = "https://buy.itunes.apple.com/verifyReceipt"
               #endif
        
        guard let receiptURL = Bundle.main.appStoreReceiptURL, let receiptString = try? Data(contentsOf: receiptURL).base64EncodedString() , let url = URL(string: urlString) else {
                return
        }
        
        let requestData : [String : Any] = ["receipt-data" : receiptString,
                                            "password" : "9927e4b6ee074b43aebc5a52d41a1888",
                                            "exclude-old-transactions" : true]
        let httpBody = try? JSONSerialization.data(withJSONObject: requestData, options: [])
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = httpBody
        URLSession.shared.dataTask(with: request)  { (data, response, error) in
            
            DispatchQueue.main.async{
            // convert data to Dictionary and view purchases
            if let data = data, let jsonData = try? JSONSerialization.jsonObject(with: data, options: .allowFragments){
                // your non-consumable and non-renewing subscription receipts are in `in_app` array
                // your auto-renewable subscription receipts are in `latest_receipt_info` array
                print(jsonData)
                if let dic = jsonData as? NSDictionary{
                    if let reciptArr = dic["latest_receipt_info"] as? NSArray{
                        if let reciptDic = reciptArr[0] as? NSDictionary{
                            if let expires_date = reciptDic["expires_date"] as? String{
                                
                                let formatter = DateFormatter()
                                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
                                if let date = formatter.date(from: expires_date) {
                                    if date > Date() {
                                        // do not save expired date to user defaults to avoid overwriting with expired date
                                        print("process done.")
                                        UserDefaults.standard.set(true, forKey: purchaseStatus)
                                        purchageStatusApp = true
                                        
                                        NotificationCenter.default.post(name: .purchageNotif, object: nil)
                                        AppDelegate.sharedAppDelegate.hideActivity()
                                    }else{
                                        UserDefaults.standard.set(false, forKey: purchaseStatus)
                                        purchageStatusApp = false
                                        AppDelegate.sharedAppDelegate.hideActivity()
                                        NotificationCenter.default.post(name: .noRestordData, object: nil)
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                print("no data")
                UserDefaults.standard.set(false, forKey: purchaseStatus)
                purchageStatusApp = false
                AppDelegate.sharedAppDelegate.hideActivity()
                NotificationCenter.default.post(name: .noRestordData, object: nil)
                }
            }
        }.resume()
    }
}

extension IPAService: SKProductsRequestDelegate{
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        print(response.products)
        self.products = response.products
        for product in response.products{
            print(product.localizedTitle)
            if product.localizedTitle == "week"{
                weekPrice = "\(product.price)"
                contryCode = "\(product.priceLocale.currencySymbol!)"
                finalWeekPrice = contryCode + weekPrice
            }else{
                monthPrice = "\(product.price)"
                contryCode = "\(product.priceLocale.currencySymbol!)"
                finalMonthPrice = contryCode + monthPrice
            }
        }
    }
}

extension IPAService: SKPaymentTransactionObserver{
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions{
            print(transaction.transactionState.status(), transaction.payment.productIdentifier)
            switch transaction.transactionState{
                
            case .purchasing:
                print("============ in purchasing ==============")
                
                break
                
            case .purchased:
                print("============ in purchased ==============")
                UserDefaults.standard.set(true, forKey: purchaseStatus)
                purchageStatusApp = true
                AppDelegate.sharedAppDelegate.hideActivity()
                SKPaymentQueue.default().finishTransaction(transaction)
                NotificationCenter.default.post(name: .purchageNotif, object: nil)
                break
            case .failed:
                print("============ in failed ==============")
                UserDefaults.standard.set(false, forKey: purchaseStatus)
                
                if let error = transaction.error{
                    let errorDesc = error.localizedDescription
                    print("transaction failed: \(errorDesc)")
                }
                
                SKPaymentQueue.default().finishTransaction(transaction)
                
                AppDelegate.sharedAppDelegate.hideActivity()
                break
                
            case .restored:
                
                self.validateReceipt()
                
                SKPaymentQueue.default().finishTransaction(transaction)
                //AppDelegate.sharedAppDelegate.hideActivity()
                break
                
            case .deferred:
                SKPaymentQueue.default().finishTransaction(transaction)
                AppDelegate.sharedAppDelegate.hideActivity()
                break
                
            @unknown default:
                queue.finishTransaction(transaction)
                AppDelegate.sharedAppDelegate.hideActivity()
            }
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, shouldAddStorePayment payment: SKPayment, for product: SKProduct) -> Bool {
        return false
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        print ("This works")
        self.validateReceipt()
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        print("restoring failed")
        AppDelegate.sharedAppDelegate.hideActivity()
    }

}

extension SKPaymentTransactionState{
    func status() -> String{
        switch self{
        case .deferred:
            return "deferred"
        case .purchasing:
            return "purchasing"
        case .purchased:
            return "purchased"
        case .failed:
            return "failed"
        case .restored:
            return "restored"
        @unknown default:
            return "default"
        }
    }
}
