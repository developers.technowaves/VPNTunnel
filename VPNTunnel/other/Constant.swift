//
//  Constant.swift
//  VPNTunnel
//
//  Created by Kartik Jasolia on 11/05/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
//

import Foundation
import UIKit
// Hard code VPN configurations
let targetBundleId = "com.approtic.secureVPN.target"
var serverAddress = "138.197.175.208"                          
var ovpnFile = "Canada"
var username = "root"
var pass = "123@Diya"
var serverImage = UIImage()

// other Data
var serverSelectFlag = false
let appName = "Welcome To secureVPN"
let purchaseStatus = "purchaseStatus"
var purchageStatusApp = false
var firstTimeFlag = "firstTimeFlag"


var weekPrice = ""
var monthPrice = ""

var contryCode = ""

var finalWeekPrice = ""
var finalMonthPrice = ""

//for select free and paid in server screen
var freeServerSelectFlag = false
var countriesData = [(name: String, flag: UIImage,countryImage:UIImage,serverAddressArr:String,serverUser:String,serverPass:String,serverName:String)]()

var blackView: UIView? = nil
func showActivityWithMessage(message: String, inView view: UIView) {
    if blackView == nil {
        blackView = UIView(frame: UIScreen.main.bounds)
        blackView!.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        var factor: Float = 1.0
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            factor = 2.0
        }
        
        var rect = CGRect.init()
        rect.size.width = CGFloat(factor * Float(200))
        rect.size.height = CGFloat(factor * Float(70))
        rect.origin.y = CGFloat((blackView?.frame.size.height)! - rect.size.height) / CGFloat(2.0)
        rect.origin.x = CGFloat((blackView?.frame.size.width)! - rect.size.width) / CGFloat(2.0)
        
        let waitingView = UIView(frame: rect)
        waitingView.backgroundColor = UIColor.clear
        waitingView.layer.cornerRadius = 8.0
        
        rect = waitingView.bounds;
        rect.size.height = CGFloat(Float(40.0) * factor);
        
        let label = UILabel(frame: rect)
        label.text = message
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: CGFloat(Float(16.0) * factor))//UIFont(name: APP_BOLD_FONT, size: CGFloat(Float(16.0) * factor))
        waitingView.addSubview(label)
        var activityIndicatorView = UIActivityIndicatorView()
        if #available(iOS 13.0, *) {
            activityIndicatorView = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.large)
        } else {
            activityIndicatorView = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.gray)
        }
        activityIndicatorView.color = UIColor.white
        
        rect = activityIndicatorView.frame;
        rect.origin.x = ((waitingView.frame.size.width - rect.size.width)/2.0);
        rect.origin.y = label.frame.size.height + 3.0;
        activityIndicatorView.frame = rect;
        
        waitingView.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        blackView?.tag = 1010
        blackView?.addSubview(waitingView)
        view.addSubview(blackView!)
    }
}
func hideActivity() {
    blackView?.removeFromSuperview()
    blackView = nil
}

func showAlet(message:String,Title:String,controller:UIViewController,finished:@escaping (Bool) -> ()) {
    let alertMessage = UIAlertAction(title: "Ok", style: .default) { (action) in
        finished(true)
    }
    
    let alert = UIAlertController(title: "\(Title)", message: "\(message)", preferredStyle: .alert)
    alert.addAction(alertMessage)
    controller.present(alert, animated: true, completion: nil)
}

func getCountryNames() {
    
    
    countriesData.append((name: "Canada", flag: #imageLiteral(resourceName: "3-white"),countryImage:#imageLiteral(resourceName: "Canada"), serverAddressArr: "138.197.175.208", serverUser: "", serverPass: "", serverName: "Canada"))
    
    countriesData.append((name: "New York", flag: #imageLiteral(resourceName: "3-pink"),countryImage:#imageLiteral(resourceName: "NewYork"), serverAddressArr: "157.245.85.83", serverUser: "", serverPass: "", serverName: "NewYork"))
    
    countriesData.append((name: "Germany", flag: #imageLiteral(resourceName: "3-white"),countryImage:#imageLiteral(resourceName: "Germany"), serverAddressArr: "161.35.20.210", serverUser: "", serverPass: "", serverName: "Frankfurt"))
    
    
    countriesData.append((name: "UK", flag: #imageLiteral(resourceName: "3-white"),countryImage:#imageLiteral(resourceName: "UK"), serverAddressArr: "178.62.64.123", serverUser: "", serverPass: "", serverName: "London"))
    
    
    
    serverAddress = countriesData[selectedContryIndex].serverAddressArr
    ovpnFile = countriesData[selectedContryIndex].serverName
    username = countriesData[selectedContryIndex].serverUser
    pass = countriesData[selectedContryIndex].serverPass
    serverImage = countriesData[selectedContryIndex].countryImage
    
    UserDefaultsManager().serverName = countriesData[selectedContryIndex].name
    UserDefaultsManager().serverAddress = serverAddress
    UserDefaultsManager().serverVPNFile = ovpnFile
    UserDefaultsManager().serverUser = username
    UserDefaultsManager().serverPassword = pass
    
    
}
var selectedContryIndex = 0

@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var UnderLineColor: UIColor? {
            get {
                return UIColor(cgColor: self.UnderLineColor!.cgColor)
            }
            set {
                let bottomLine = UIView()
                bottomLine.frame = CGRect(x: 0, y: self.frame.height - 1, width:self.bounds.width, height: 1.0)
                bottomLine.backgroundColor = UIColor.white
                self.borderStyle = UITextField.BorderStyle.none
                self.clipsToBounds = true
                self.addSubview(bottomLine)
            }
        }
    
    @IBInspectable var leftPadding: CGFloat = 0
    
    @IBInspectable var PlaceHoldercolor: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var imageWidth: Double  = 30 {
            didSet {
                    updateView()
                }
    }
    
    @IBInspectable var imageHeight: Double  = 30 {
                didSet {
                        updateView()
                    }
        }
    
    func updateView() {
        if let image = leftImage {
            
            let leftImageView = UIView()
            
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 10, y: 4, width: imageWidth, height: imageHeight))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = PlaceHoldercolor
            leftImageView.addSubview(imageView)
            leftImageView.frame = CGRect(x: 0, y: 0, width: imageWidth + 20, height: imageHeight + 8)
            leftView = leftImageView
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: PlaceHoldercolor])
    }
    
   
}
