//
//  extension.swift
//  dualWhatsUp
//
//  Created by Kartik Jasolia on 01/05/20.
//  Copyright © 2020 ViralKumar Goti. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension NSNotification.Name{
    static let purchageNotif = Notification.Name("purchageNotif")
    static let noRestordData = Notification.Name("noRestordData")
}
