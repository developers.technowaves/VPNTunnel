//
//  ZWView.swift

import UIKit
@IBDesignable
class zwView : UIImageView {
    var topBorder: UIView?
    var bottomBorder: UIView?
    var leftBorder: UIView?
    var rightBorder: UIView?
    var shadowLayer: CAShapeLayer!
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
           // layer.masksToBounds = true
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
            //layer.masksToBounds = true
        }
    }

    @IBInspectable var shadowone : Bool = false {
        didSet{
//            layer.shadowColor = UIColor.black.cgColor
//            layer.shadowOpacity = 0.06
//            layer.shadowOffset = CGSize.zero
//            layer.shadowRadius = 12.0
//
//            layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
           // layer.shouldRasterize = true
            
//            shadowLayer = CAShapeLayer()
//            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 35).cgPath
//            shadowLayer.fillColor = UIColor.clear.cgColor
//
//            shadowLayer.shadowColor = UIColor.darkGray.cgColor
//            shadowLayer.shadowPath = shadowLayer.path
//            shadowLayer.shadowOffset = CGSize(width: 2.0, height: 15.0)
//            shadowLayer.shadowOpacity = 0.08
//            shadowLayer.shadowRadius = 8
//
//            layer.insertSublayer(shadowLayer, at: 0)
            
            shadowLayer = CAShapeLayer()
           // layer.masksToBounds = false
            shadowLayer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 12.0)
            shadowLayer.shadowRadius = 12.0
            shadowLayer.shadowOpacity = 0.2
            let trianglePath = UIBezierPath()
            
            trianglePath.move(to: CGPoint(x: 15, y: 0))
            trianglePath.addLine(to: CGPoint(x: 15, y: frame.size.height))
            trianglePath.addLine(to: CGPoint(x: frame.size.width - 15, y: frame.size.height))
            trianglePath.addLine(to: CGPoint(x: frame.size.width - 15, y: 0))
            trianglePath.close()
            
            shadowLayer.shadowPath = trianglePath.cgPath
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
    @IBInspectable var shadowtwo : Bool = false {
        didSet{
            //            layer.shadowColor = UIColor.black.cgColor
            //            layer.shadowOpacity = 0.06
            //            layer.shadowOffset = CGSize.zero
            //            layer.shadowRadius = 12.0
            //
            //            layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
            // layer.shouldRasterize = true
            
            //            shadowLayer = CAShapeLayer()
            //            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 35).cgPath
            //            shadowLayer.fillColor = UIColor.clear.cgColor
            //
            //            shadowLayer.shadowColor = UIColor.darkGray.cgColor
            //            shadowLayer.shadowPath = shadowLayer.path
            //            shadowLayer.shadowOffset = CGSize(width: 2.0, height: 15.0)
            //            shadowLayer.shadowOpacity = 0.08
            //            shadowLayer.shadowRadius = 8
            //
            //            layer.insertSublayer(shadowLayer, at: 0)
            
            shadowLayer = CAShapeLayer()
            // layer.masksToBounds = false
            shadowLayer.shadowColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 8.0)
            shadowLayer.shadowRadius = 8
            shadowLayer.shadowOpacity = 0.6
            let trianglePath = UIBezierPath()
                    
            trianglePath.move(to: CGPoint(x: 20, y: 0))
            trianglePath.addLine(to: CGPoint(x: 20, y: frame.size.height - 40))
            trianglePath.addArc(withCenter: CGPoint(x: 40, y: frame.size.height - 20), radius: 20, startAngle: CGFloat.pi, endAngle: CGFloat.pi / 2, clockwise: false)
            trianglePath.addArc(withCenter: CGPoint(x: UIScreen.main.bounds.width/1.5 - 60, y: frame.size.height - 20), radius: 20, startAngle: CGFloat.pi / 2, endAngle: 0, clockwise: false)
            trianglePath.addLine(to: CGPoint(x: UIScreen.main.bounds.width/1.5 - 40, y: 0))
            trianglePath.close()
            
            shadowLayer.shadowPath = trianglePath.cgPath
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
    @IBInspectable var shadowthree : Bool = false {
        didSet{
            
            shadowLayer = CAShapeLayer()
            shadowLayer.shadowColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 8.0)
            shadowLayer.shadowRadius = 8
            shadowLayer.shadowOpacity = 0.4
            let trianglePath = UIBezierPath()
            
            trianglePath.move(to: CGPoint(x: 15, y: 0))
            trianglePath.addLine(to: CGPoint(x: 20, y: UIScreen.main.bounds.width / 1.6))
            trianglePath.addLine(to: CGPoint(x: UIScreen.main.bounds.width / 2.8 , y: UIScreen.main.bounds.width / 1.6))
            trianglePath.addLine(to: CGPoint(x: UIScreen.main.bounds.width / 2.8 , y: 0))
            trianglePath.close()

            shadowLayer.shadowPath = trianglePath.cgPath
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
    @IBInspectable var shadowColor: UIColor = UIColor.clear {
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0.0 {
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0.0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var masksToBounds: Bool = true {
        didSet {
            layer.masksToBounds = masksToBounds
        }
    }
    
    @IBInspectable var shadowOffset: CGSize = CGSize(width: 0.0, height: 0.0) {
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var gradientNo: Int = 0
    
    @IBInspectable var topColor: UIColor = UIColor.clear {
        didSet {
              self.setNeedsLayout()
        }
    }
    
    @IBInspectable var bottomColor: UIColor = UIColor.clear {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        //gradientLayer.colors = [bottomColor.cgColor, topColor.cgColor, bottomColor.cgColor]
        //gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        //gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        //gradientLayer.locations = [0.0,1.0]
        //gradientLayer.frame = self.bounds
        //self.layer.insertSublayer(gradientLayer, at: 0)
        
        if(gradientNo == 0){
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        }else if(gradientNo == 1){
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
        }
        
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
